import React from 'react';
import { PrejtComponent, Items, ComponentConfig } from '@prejt/ui-core';
import {
	Table,
	TableBody,
	TableHead,
	TableRow,
	TableCell
} from '@material-ui/core';
import { withLayout } from '../../hoc';

type GridProps = {
	items: string;
	childConfigs: ComponentConfig<unknown>[];
};

const GridComponent: PrejtComponent<GridProps> = ({
	items,
	childConfigs,
	parentDataPath,
	eventContext
}) => {
	const row = childConfigs.find(config => config.name === 'grid-row');
	const hasHead =
		row &&
		row.childConfigs &&
		row.childConfigs.some(
			col => !!(col.attributes && (col.attributes as any).label)
		);
	return (
		<Table>
			{hasHead && (
				<TableHead>
					<TableRow>
						{row &&
							row.childConfigs &&
							row.childConfigs.map((col, index) => (
								<TableCell key={index}>
									{(col.attributes as any).label}
								</TableCell>
							))}
					</TableRow>
				</TableHead>
			)}
			<TableBody>
				<Items
					childConfigs={childConfigs}
					items={items}
					dataPath={parentDataPath}
					eventContext={eventContext}
				/>
			</TableBody>
		</Table>
	);
};

export const Grid = withLayout(GridComponent);

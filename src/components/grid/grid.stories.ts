import { Grid } from './grid';
import { prejtStory, getItems } from '../../utils';

export default {
	title: 'Grid',
	component: Grid
};

const data = {
	items: getItems(8)
};

export const WithItemsFromDataStore = prejtStory(
	[
		{
			name: 'grid',
			attributes: {
				items: '/items'
			},
			childConfigs: [
				{
					name: 'grid-row',
					attributes: {},
					childConfigs: [
						{
							name: 'grid-col',
							attributes: {
								label: 'Value'
							},
							childConfigs: [
								{
									name: 'string',
									attributes: {
										value: './value'
									}
								}
							]
						},
						{
							name: 'grid-col',
							attributes: {
								label: 'Name'
							},
							childConfigs: [
								{
									name: 'string',
									attributes: {
										value: './name'
									}
								}
							]
						}
					]
				}
			]
		}
	],
	data
);

export const WithoutHead = prejtStory(
	[
		{
			name: 'grid',
			attributes: {
				items: '/items'
			},
			childConfigs: [
				{
					name: 'grid-row',
					attributes: {},
					childConfigs: [
						{
							name: 'grid-col',
							attributes: {},
							childConfigs: [
								{
									name: 'string',
									attributes: {
										value: './value'
									}
								}
							]
						},
						{
							name: 'grid-col',
							attributes: {},
							childConfigs: [
								{
									name: 'string',
									attributes: {
										value: './name'
									}
								}
							]
						}
					]
				}
			]
		}
	],
	data
);

export const WithItemsAsChildren = prejtStory([
	{
		name: 'grid',
		attributes: {},
		childConfigs: [
			{
				name: 'grid-row',
				attributes: {},
				childConfigs: [
					{
						name: 'grid-col',
						attributes: {
							label: 'Value'
						},
						childConfigs: [
							{
								name: 'string',
								attributes: {
									value: '1'
								}
							}
						]
					},
					{
						name: 'grid-col',
						attributes: {
							label: 'Name'
						},
						childConfigs: [
							{
								name: 'string',
								attributes: {
									value: 'Child 1'
								}
							}
						]
					}
				]
			},
			{
				name: 'grid-row',
				attributes: {},
				childConfigs: [
					{
						name: 'grid-col',
						attributes: {},
						childConfigs: [
							{
								name: 'string',
								attributes: {
									value: '2'
								}
							}
						]
					},
					{
						name: 'grid-col',
						attributes: {},
						childConfigs: [
							{
								name: 'string',
								attributes: {
									value: 'Child 2'
								}
							}
						]
					}
				]
			}
		]
	}
]);

export const WithInputs = prejtStory(
	[
		{
			name: 'grid',
			attributes: {
				items: '/items'
			},
			childConfigs: [
				{
					name: 'grid-row',
					attributes: {},
					childConfigs: [
						{
							name: 'grid-col',
							attributes: {},
							childConfigs: [
								{
									name: 'string',
									attributes: {
										value: './id'
									}
								}
							]
						},
						{
							name: 'grid-col',
							attributes: {},
							childConfigs: [
								{
									name: 'text',
									attributes: {
										value: './id',
										label: 'Test'
									}
								}
							]
						}
					]
				}
			]
		}
	],
	{
		items: [
			{
				id: 1,
				value: 'test 1'
			},
			{
				id: 2,
				value: 'test 2'
			}
		]
	}
);

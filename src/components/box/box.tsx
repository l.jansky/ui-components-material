import React from 'react';
import {
	PrejtComponent,
	useStoreDataPath,
	Container,
	getAbsolutePathArray
} from '@prejt/ui-core';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import { withLayout } from '../../hoc/with-layout/with-layout';

type BoxProps = {
	label: string;
	data?: string;
	module?: string;
	visible?: string;
};

const useStyles = makeStyles(theme => ({
	root: {
		...theme.mixins.gutters(),
		paddingTop: theme.spacing(2),
		paddingBottom: theme.spacing(2),
		margin: theme.spacing(1),
		position: 'relative'
	}
}));

const BoxComponent: PrejtComponent<BoxProps> = ({
	childConfigs = [],
	label,
	data = './',
	parentDataPath,
	module,
	eventContext,
	visible
}) => {
	const dataPath = getAbsolutePathArray(data, parentDataPath);
	const [labelValue] = useStoreDataPath(label, parentDataPath, eventContext);
	const [moduleValue] = useStoreDataPath(module, parentDataPath, eventContext);
	const [visibleValue] = useStoreDataPath(
		visible,
		parentDataPath,
		eventContext
	);
	const classes = useStyles();
	const display = !visible || visibleValue ? 'block' : 'none';
	return (
		<Paper className={classes.root} style={{ display }}>
			<Typography variant="h3">{labelValue}</Typography>
			<Container
				childConfigs={childConfigs}
				dataPath={dataPath}
				module={moduleValue as string}
				eventContext={eventContext}
			/>
		</Paper>
	);
};

export const Box = withLayout(BoxComponent);

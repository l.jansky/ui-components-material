import { ComponentConfig } from '@prejt/ui-core';
import { Box } from './box';
import { prejtStory, getTestContent } from '../../utils';

const boxModuleResponse: ComponentConfig = {
	name: 'module',
	attributes: {},
	childConfigs: [
		{
			name: 'box',
			attributes: {
				label: 'Inner box loaded from module'
			}
		}
	]
};

export default {
	title: 'Box',
	component: Box
};

export const WithContentFromChildren = prejtStory(
	[
		{
			name: 'box',
			attributes: {
				label: '/label'
			},
			childConfigs: [
				{
					name: 'box',
					attributes: {
						label: 'Inner box from childConfigs'
					}
				}
			]
		}
	],
	{ label: 'Outer box' }
);

export const WithContentFromModule = prejtStory(
	[
		{
			name: 'box',
			attributes: {
				label: '/label',
				module: 'http://localhost/box-module'
			}
		}
	],
	{ label: 'Outer box' }
).fetchMock('http://localhost/box-module', { body: boxModuleResponse });

export const WithRandomContent = prejtStory([
	{
		name: 'box',
		attributes: {
			label: 'Random content'
		},
		childConfigs: getTestContent(30)
	}
]);

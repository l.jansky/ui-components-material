import { withEnabled } from '@prejt/ui-core';
import { Box } from './box/box';
import { Menu } from './menu/menu';
import { MenuItem } from './menu-item/menu-item';
import { Button } from './button/button';
import { AppBar } from './app-bar/app-bar';
import { Text } from './text/text';
import { Checkbox } from './checkbox/checkbox';
import { Select } from './select/select';
import { Option } from './option/option';
import { Layout } from './layout/layout';
import { Grid } from './grid/grid';
import { GridRow } from './grid-row/grid-row';
import { GridCol } from './grid-col/grid-col';
import { DateComponent } from './date/date';
import { DateTime } from './date-time/date-time';
import { Drawer } from './drawer/drawer';
import { Tabs } from './tabs/tabs';
import { Autocomplete } from './autocomplete/autocomplete';
import { Switch } from './switch/switch';
import { Dialog } from './dialog/dialog';
import { FetchAutocomplete } from './fetch-autocomplete/fetch-autocomplete';
import { EditableGrid } from './editable-grid/editable-grid';
import { EditableList } from './editable-list/editable-list';
import { Toast } from './toast/toast';
import { List } from './list/list';
import { Typography } from './typography/typography';
export * from './mui-components-provider/mui-components-provider';

const baseComponents = {
	box: Box,
	menu: Menu,
	'menu-item': MenuItem,
	button: Button,
	'app-bar': AppBar,
	text: Text,
	checkbox: Checkbox,
	select: Select,
	option: Option,
	layout: Layout,
	grid: Grid,
	'grid-row': GridRow,
	'grid-col': GridCol,
	date: DateComponent,
	'date-time': DateTime,
	drawer: Drawer,
	tabs: Tabs,
	autocomplete: Autocomplete,
	switch: Switch,
	dialog: Dialog,
	'fetch-autocomplete': FetchAutocomplete,
	'editable-grid': EditableGrid,
	'editable-list': EditableList,
	toast: Toast,
	list: List,
	typography: Typography
};

export const components = Object.assign(
	{},
	...Object.entries(baseComponents).map(([k, component]) => ({
		[k]: withEnabled(component)
	}))
);

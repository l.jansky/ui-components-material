import React from 'react';
import { PrejtComponent, useStoreDataPath } from '@prejt/ui-core';
import { Snackbar, SnackbarContent, Icon, makeStyles } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
	success: {
		backgroundColor: theme.palette.success.main
	},
	error: {
		backgroundColor: theme.palette.error.main
	},
	info: {
		backgroundColor: theme.palette.info.main
	},
	warning: {
		backgroundColor: theme.palette.warning.main
	},
	message: {
		display: 'flex',
		alignItems: 'center'
	},
	messageIcon: {
		marginRight: theme.spacing(1)
	}
}));

const iconVariants = {
	success: 'check-circle',
	error: 'exclamation-circle',
	info: 'info-circle',
	warning: 'exclamation-triangle'
};

type ToastProps = {
	message: string;
	variant?: 'success' | 'error' | 'info' | 'warning';
};

export const Toast: PrejtComponent<ToastProps> = ({
	message,
	parentDataPath,
	eventContext,
	variant
}) => {
	const classes = useStyles();
	const [messageValue] = useStoreDataPath(
		message,
		parentDataPath,
		eventContext
	);

	const icon =
		variant && iconVariants[variant] ? (
			<Icon
				className={`fa fa-${iconVariants[variant]} ${classes.messageIcon}`}
			/>
		) : null;

	return (
		<Snackbar
			anchorOrigin={{
				vertical: 'bottom',
				horizontal: 'left'
			}}
			open={true}
			key={messageValue as string}
			autoHideDuration={6000}
		>
			<SnackbarContent
				className={(variant && classes[variant]) || undefined}
				message={
					<span className={classes.message}>
						{icon} {messageValue}
					</span>
				}
			/>
		</Snackbar>
	);
};

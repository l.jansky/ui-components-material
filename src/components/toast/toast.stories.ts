import { prejtStory } from '../../utils';

export default {
	title: 'Toast'
};

export const Variants = prejtStory([
	{
		name: 'button',
		attributes: {
			label: 'Show default',
			onClick: 'value(Test Message)|toast'
		}
	},
	{
		name: 'button',
		attributes: {
			label: 'Show success',
			onClick: 'value(Test Message)|toast(success)'
		}
	},
	{
		name: 'button',
		attributes: {
			label: 'Show error',
			onClick: 'value(Test Message)|toast(error)'
		}
	},
	{
		name: 'button',
		attributes: {
			label: 'Show info',
			onClick: 'value(Test Message)|toast(info)'
		}
	},
	{
		name: 'button',
		attributes: {
			label: 'Show warning',
			onClick: 'value(Test Message)|toast(warning)'
		}
	}
]);

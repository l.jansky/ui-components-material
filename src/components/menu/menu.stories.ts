import { ComponentConfig } from '@prejt/ui-core';
import { Menu } from './menu';
import { prejtStory, getItems } from '../../utils';

export default {
	title: 'Menu',
	component: Menu
};

const items = getItems(5, { label: 'Menu item' });

export const WithItemsFromChildren = prejtStory(
	[
		{
			name: 'drawer',
			attributes: {
				visible: '/opened',
				anchor: 'left'
			},
			childConfigs: [
				{
					name: 'menu',
					attributes: {},
					childConfigs: items.map<ComponentConfig>(({ label }, index) => ({
						name: 'menu-item',
						attributes: {
							label: label as string,
							icon: (index % 2 === 0 ? '/icon' : undefined) as string,
							onClick: 'action(menu-item-click)'
						}
					}))
				}
			]
		}
	],
	{
		opened: true,
		icon: 'user'
	}
);

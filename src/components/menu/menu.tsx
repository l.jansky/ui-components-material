import React from 'react';
import { PrejtComponent, Items, ComponentConfig } from '@prejt/ui-core';
import { List } from '@material-ui/core';
import { withLayout } from '../../hoc';

type MenuProps = {
	items: string;
	childConfigs: ComponentConfig<unknown>[];
};

const MenuComponent: PrejtComponent<MenuProps> = ({
	items,
	childConfigs,
	parentDataPath,
	eventContext
}) => {
	return (
		<List>
			<Items
				childConfigs={childConfigs}
				items={items}
				dataPath={parentDataPath}
				eventContext={eventContext}
			/>
		</List>
	);
};

export const Menu = withLayout(MenuComponent);

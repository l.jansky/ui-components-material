import React from 'react';
import { PrejtComponent, useStoreDataPath } from '@prejt/ui-core';

type OptionProps = {
	value: string;
	label: string;
};

export const Option: PrejtComponent<OptionProps> = ({
	value,
	parentDataPath,
	label,
	eventContext
}) => {
	const [dataValue] = useStoreDataPath(value, parentDataPath, eventContext);
	const [labelValue] = useStoreDataPath(label, parentDataPath, eventContext);
	return <option value={dataValue as any}>{labelValue as string}</option>;
};

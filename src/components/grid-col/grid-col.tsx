import React from 'react';
import {
	PrejtComponent,
	useStoreDataPath,
	Container,
	getAbsolutePathArray
} from '@prejt/ui-core';
import {
	TableCell,
	Grid,
	GridDirection,
	GridJustification,
	GridItemsAlignment
} from '@material-ui/core';

type GridColProps = {
	data?: string;
	module?: string;
	label?: string;
	direction?: GridDirection;
	justify?: GridJustification;
	alignItems?: GridItemsAlignment;
	spacing?: number;
};

export const GridCol: PrejtComponent<GridColProps> = ({
	childConfigs = [],
	parentDataPath,
	data = './',
	module,
	eventContext,
	direction,
	justify,
	alignItems,
	spacing = 3
}) => {
	const dataPath = getAbsolutePathArray(data, parentDataPath);
	const [moduleValue] = useStoreDataPath(module, parentDataPath, eventContext);

	return (
		<TableCell>
			<Grid
				container={true}
				spacing={spacing as any}
				direction={direction}
				justify={justify}
				alignItems={alignItems}
			>
				<Container
					childConfigs={childConfigs}
					dataPath={dataPath}
					module={moduleValue as string}
					eventContext={eventContext}
				/>
			</Grid>
		</TableCell>
	);
};

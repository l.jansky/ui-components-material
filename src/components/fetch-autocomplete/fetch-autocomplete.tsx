import React from 'react';
import { uuid } from 'uuidv4';
import { PrejtComponent, DEFAULT_COMPONENTS } from '@prejt/ui-core';
import { AutocompleteComponent } from '../autocomplete/autocomplete';
import { withLayout } from '../../hoc';

const Fetcher = DEFAULT_COMPONENTS['fetcher'];
const Value = DEFAULT_COMPONENTS['value'];

type FetchAutocompleteProps = {
	label: string;
	value: string;
	endpoint: string;
	optionLabel?: string;
	onSearch?: string;
	onAdd?: string;
	onChange?: string;
};

const FetchAutocompleteComponent: PrejtComponent<FetchAutocompleteProps> = ({
	label,
	value,
	endpoint,
	optionLabel = 'label',
	onSearch,
	onAdd,
	parentDataPath,
	eventContext,
	childConfigs = []
}) => {
	const [selectorId] = React.useState(uuid());

	let onSearchPipe = `store(/selectors/${selectorId}/query/${optionLabel})`;
	if (onSearch) {
		onSearchPipe = onSearch.replace('@path', `/selectors/${selectorId}`);
	}

	return (
		<>
			<Value
				parentDataPath={parentDataPath}
				eventContext={eventContext}
				childConfigs={childConfigs}
				value={value}
				onChange={`store(/selectors/${selectorId}/params/id)`}
			/>
			<Fetcher
				parentDataPath={parentDataPath}
				eventContext={eventContext}
				childConfigs={childConfigs}
				fetch={`${endpoint}/:id`}
				onData={`store(/selectors/${selectorId}/selected)`}
				params={`/selectors/${selectorId}/params`}
				autofetch={'true'}
				enabled={`/selectors/${selectorId}/params/id`}
			/>
			<Fetcher
				parentDataPath={parentDataPath}
				eventContext={eventContext}
				childConfigs={childConfigs}
				fetch={endpoint}
				onData={`store(/selectors/${selectorId}/items)`}
				query={`/selectors/${selectorId}/query`}
				autofetch={'true'}
				debounce={'200'}
			/>
			<AutocompleteComponent
				parentDataPath={parentDataPath}
				eventContext={eventContext}
				childConfigs={childConfigs}
				label={label}
				value={value}
				items={`/selectors/${selectorId}/items`}
				selected={`/selectors/${selectorId}/selected/${optionLabel}`}
				onSearch={onSearchPipe}
				onAdd={onAdd}
			/>
		</>
	);
};

export const FetchAutocomplete = withLayout(FetchAutocompleteComponent, true);

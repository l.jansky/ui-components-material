import { FetchAutocomplete } from './fetch-autocomplete';
import { prejtStory, getItems } from '../../utils';

export default {
	title: 'Fetch autocomplete',
	component: FetchAutocomplete
};

const options = getItems(5, { value: '', label: 'Option' });

export const WithoutOnChange = prejtStory([
	{
		name: 'fetch-autocomplete',
		attributes: {
			label: 'Autocomplete label',
			value: '/value',
			endpoint: '//options'
		},
		childConfigs: [
			{
				name: 'option',
				attributes: {
					value: './value',
					label: './label'
				}
			}
		]
	}
])
	.fetchMock('express:/options/:value', (url: string) => {
		const value = parseInt(url.split('/').pop() || '0');
		return { body: options[value] };
	})
	.fetchMock('express:/options', (url: string) => {
		const search = url.split('=').pop() || '';
		const body = options.filter(option =>
			option.label.toString().includes(search)
		);
		return { body };
	});

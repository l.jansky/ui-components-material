import { EditableList } from './editable-list';
import { prejtStory } from '../../utils';

export default {
	title: 'EditableList',
	component: EditableList
};

const data = {
	items: ['test1', 'test2', 'test3']
};

export const WithItemsFromDataStore = prejtStory(
	[
		{
			name: 'editable-list',
			attributes: {
				items: '/items'
			},
			childConfigs: [
				{
					name: 'list-item',
					attributes: {},
					childConfigs: [
						{
							name: 'text',
							attributes: {
								value: './',
								label: 'Test text',
								xs: '12'
							}
						}
					]
				}
			]
		}
	],
	data
);

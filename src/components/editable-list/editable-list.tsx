import React from 'react';
import {
	PrejtComponent,
	ComponentConfig,
	Container,
	useItems,
	useEvents,
	useStoreDataPath,
	useStoreValue,
	useInvokePipe,
	getPipeIO
} from '@prejt/ui-core';
import {
	List,
	ListItem,
	ListItemSecondaryAction,
	IconButton,
	Icon,
	Grid
} from '@material-ui/core';
import { uuid } from 'uuidv4';
import { withLayout } from '../../hoc';

type EditableListProps = {
	items: string;
	onNewChange?: string;
	addOnEvent?: string;
	childConfigs: ComponentConfig<unknown>[];
};

const EditableListComponent: PrejtComponent<EditableListProps> = ({
	items,
	childConfigs,
	parentDataPath,
	eventContext,
	onNewChange = '',
	addOnEvent
}) => {
	const [componentId] = React.useState(uuid());
	const newItemPath = ['selectors', componentId, 'newItem'];
	const [itemsData, setItemsData] = useStoreDataPath(
		items,
		parentDataPath,
		eventContext
	);
	const [newItemData, setNewItemData] = useStoreValue(newItemPath);
	const itemConfigs = useItems(childConfigs, items, parentDataPath);
	const invokeNewOnChangePipe = useInvokePipe(
		onNewChange,
		parentDataPath,
		eventContext
	);

	React.useEffect(() => {
		if (typeof newItemData !== 'undefined') {
			invokeNewOnChangePipe(getPipeIO(newItemData));
		}
	}, [newItemData]);

	const itemsDataArray = Array.isArray(itemsData) ? itemsData : [];

	useEvents((addOnEvent && addOnEvent.split(',')) || [], eventContext, data => {
		addNewItem(data);
	});

	const newItemRef = React.useRef<HTMLInputElement>(null);

	const addNewItem = (data: any) => {
		if (data) {
			setItemsData([...itemsDataArray, data]);
			setNewItemData(undefined);
			setTimeout(() => {
				const inputs = newItemRef.current?.getElementsByTagName('input');
				if (inputs && inputs[0]) {
					inputs[0].focus();
				}
			}, 0);
		}
	};

	const removeItem = (key: number) => {
		const newItemsData = itemsDataArray.filter((_, itemKey) => itemKey !== key);
		setItemsData(newItemsData);
	};

	return (
		<List>
			{itemConfigs.map(({ dataPath, childConfigs, key, attributes }) => (
				<ListItem key={key} disableGutters={true}>
					<Grid
						container={true}
						spacing={attributes.spacing}
						direction={attributes.direction}
						justify={attributes.justify}
						alignItems={attributes.alignItems}
					>
						<Container
							childConfigs={childConfigs || []}
							dataPath={dataPath}
							eventContext={eventContext}
						/>
					</Grid>
					<ListItemSecondaryAction>
						<IconButton
							color={'secondary'}
							size={'small'}
							onClick={() => removeItem(key)}
						>
							<Icon fontSize={'small'} className={`fa fa-trash`} />
						</IconButton>
					</ListItemSecondaryAction>
				</ListItem>
			))}
			<ListItem key={itemConfigs.length} disableGutters={true}>
				<Grid container={true} ref={newItemRef}>
					<Container
						childConfigs={childConfigs[0]?.childConfigs || []}
						dataPath={newItemPath}
						eventContext={eventContext}
					/>
				</Grid>
				{!addOnEvent && (
					<ListItemSecondaryAction>
						<IconButton
							color={'primary'}
							size={'small'}
							onClick={() => addNewItem(newItemData)}
						>
							<Icon fontSize={'small'} className={`fa fa-plus`} />
						</IconButton>
					</ListItemSecondaryAction>
				)}
			</ListItem>
		</List>
	);
};

export const EditableList = withLayout(EditableListComponent);

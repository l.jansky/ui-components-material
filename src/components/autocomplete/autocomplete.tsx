import React from 'react';
import {
	PrejtComponent,
	useStoreDataPath,
	ComponentConfig,
	useItems,
	PathSegment,
	useInvokePipe,
	EventContext,
	getPipeIO
} from '@prejt/ui-core';
import {
	TextField,
	makeStyles,
	Paper,
	MenuItem,
	Popper,
	Container,
	ClickAwayListener,
	Grow,
	InputAdornment,
	IconButton,
	Icon,
	List
} from '@material-ui/core';
import { withLayout } from '../../hoc';

type AutocompleteProps = {
	label: string;
	value: string;
	selected: string;
	onSearch?: string;
	onAdd?: string;
	onChange?: string;
	items?: string;
	childConfigs: ComponentConfig<unknown>[];
};

const useStyles = makeStyles(theme => ({
	popper: {
		zIndex: 1500
	}
}));

type AutocompleteOptionProps = {
	dataPath: PathSegment[];
	onSelect: (value: any) => void;
	onFocus: (value: any) => void;
	value: string;
	label: string;
	selected: boolean;
	context: EventContext;
};

const AutocompleteOption: React.FC<AutocompleteOptionProps> = ({
	dataPath,
	onSelect,
	onFocus,
	value,
	label,
	selected,
	context
}) => {
	const [itemValue] = useStoreDataPath(value, dataPath, context);
	const [labelValue] = useStoreDataPath(label, dataPath, context);
	const onClick = React.useCallback(() => {
		onSelect(itemValue);
	}, [itemValue]);

	React.useEffect(() => {
		if (selected) {
			onFocus(itemValue);
		}
	}, [selected, itemValue]);

	return (
		<MenuItem onClick={onClick} selected={selected}>
			{labelValue}
		</MenuItem>
	);
};

export const AutocompleteComponent: PrejtComponent<AutocompleteProps> = ({
	label,
	parentDataPath,
	value,
	items,
	onSearch = '',
	selected,
	onAdd = '',
	onChange = '',
	childConfigs,
	eventContext
}) => {
	const [labelValue] = useStoreDataPath(label, parentDataPath, eventContext);
	const [dataValue, setDataValue] = useStoreDataPath(
		value,
		parentDataPath,
		eventContext
	);
	const [searchValue, setSearchValue] = React.useState('');
	const [selectedValue] = useStoreDataPath(
		selected,
		parentDataPath,
		eventContext
	);
	const [suggestionsVisible, setSuggestionsVisible] = React.useState(false);
	const itemsConfigs = useItems(childConfigs, items, parentDataPath);
	const classes = useStyles();
	const invokeOnChangePipe = useInvokePipe(
		onChange,
		parentDataPath,
		eventContext
	);

	const onSelect = React.useCallback(value => {
		if (onChange) {
			invokeOnChangePipe(getPipeIO(value));
		} else {
			setDataValue(value);
		}
	}, []);
	const [focusedOptionIndex, setFocusedOptionIndex] = React.useState(0);
	const [focusedOptionValue, setFocusedOptionValue] = React.useState();

	const invokeOnAddPipe = useInvokePipe(onAdd, parentDataPath, eventContext);
	const invokeOnSearchPipe = useInvokePipe(
		onSearch,
		parentDataPath,
		eventContext
	);

	React.useEffect(() => {
		setSuggestionsVisible(false);
		setSearchValue('');
	}, [dataValue]);

	React.useEffect(() => {
		setFocusedOptionIndex(0);
		if (searchValue) {
			invokeOnSearchPipe({ value: searchValue });
		}
	}, [searchValue]);

	React.useEffect(() => {
		if (suggestionsVisible && searchValue && itemsConfigs.length === 1) {
			onSelect(focusedOptionValue);
		}
	}, [focusedOptionValue]);

	const anchorRef = React.useRef<HTMLInputElement>(null);
	const searchRef = React.useRef<HTMLInputElement>(null);

	const handleOpen = (e: React.FocusEvent) => {
		setTimeout(() => {
			setSearchValue('');
			setSuggestionsVisible(true);
			searchRef.current?.focus();
		}, 100);
	};

	const handleClose = (e: React.MouseEvent<Document, MouseEvent>) => {
		if (!anchorRef.current || !anchorRef.current.contains(e.target as Node)) {
			setSuggestionsVisible(false);
			setSearchValue('');
		}
	};

	const handleAdd = (event: any) => {
		event.preventDefault();
		event.stopPropagation();
		invokeOnAddPipe({ value: undefined });
	};

	return (
		<>
			<TextField
				label={labelValue}
				value={selectedValue || ''}
				onFocus={handleOpen}
				margin="normal"
				fullWidth={true}
				ref={anchorRef}
				InputProps={
					onAdd
						? {
								endAdornment: (
									<InputAdornment position="end">
										<IconButton onClick={handleAdd}>
											<Icon className={`fa fa-plus-circle`} />
										</IconButton>
									</InputAdornment>
								)
						  }
						: {}
				}
			/>

			<Popper
				anchorEl={anchorRef.current}
				open={suggestionsVisible}
				transition
				className={classes.popper}
			>
				{({ TransitionProps, placement }) => (
					<Grow
						{...TransitionProps}
						style={{
							transformOrigin:
								placement === 'bottom' ? 'center top' : 'center bottom'
						}}
					>
						<Paper>
							<ClickAwayListener onClickAway={handleClose}>
								<Container>
									<TextField
										label={'Search'}
										value={searchValue || ''}
										onChange={e => setSearchValue(e.target.value)}
										margin="normal"
										fullWidth={true}
										onFocus={() => setSearchValue('')}
										inputRef={searchRef}
										onKeyDownCapture={event => {
											switch (event.key) {
												case 'Enter':
													event.preventDefault();
													event.stopPropagation();
													onSelect(focusedOptionValue);
													break;
												case 'ArrowUp':
													event.preventDefault();
													event.stopPropagation();
													setFocusedOptionIndex(prevState =>
														prevState > 0 ? prevState - 1 : 0
													);
													break;
												case 'ArrowDown':
													event.preventDefault();
													event.stopPropagation();
													setFocusedOptionIndex(prevState =>
														prevState < itemsConfigs.length - 1
															? prevState + 1
															: prevState
													);
													break;
											}
										}}
									/>
									<List>
										{itemsConfigs.map(
											({ dataPath, attributes: { value, label } }, index) => (
												<AutocompleteOption
													key={index}
													selected={focusedOptionIndex === index}
													dataPath={dataPath}
													onSelect={onSelect}
													onFocus={setFocusedOptionValue}
													value={value}
													label={label}
													context={eventContext}
												/>
											)
										)}
									</List>
								</Container>
							</ClickAwayListener>
						</Paper>
					</Grow>
				)}
			</Popper>
		</>
	);
};

export const Autocomplete = withLayout(AutocompleteComponent, true);

import { Autocomplete } from './autocomplete';
import { prejtStory, getItems } from '../../utils';
import { ComponentConfig } from '@prejt/ui-core';

export default {
	title: 'Autocomplete',
	component: Autocomplete
};

const options = getItems(5, { value: '', label: 'Option' });

const fetchOption = (url: string) => {
	const value = parseInt(url.split('/').pop() || '0');
	return { body: options[value] };
};

const fetchOptions = (url: string) => {
	const search = url.split('=').pop() || '';
	const body = options.filter(option =>
		option.label.toString().includes(search)
	);
	return { body: options };
};

const autocompleteFetchers: ComponentConfig[] = [
	{
		name: 'value',
		attributes: {
			value: '/value',
			onChange: 'store(/params/value)'
		}
	},
	{
		name: 'fetcher',
		attributes: {
			fetch: '//options/:value',
			onData: 'store(/selected)',
			params: '/params',
			autofetch: 'true',
			enabled: '/params/value'
		}
	},
	{
		name: 'fetcher',
		attributes: {
			fetch: '//options',
			onData: 'store(/options)',
			query: '/query',
			autofetch: 'true',
			debounce: '200'
		}
	}
];

export const WithoutOnChange = prejtStory([
	...autocompleteFetchers,
	{
		name: 'autocomplete',
		attributes: {
			label: 'Autocomplete label',
			value: '/value',
			items: '/options',
			onSearch: 'store(/query/search)',
			selected: '/selected/label'
		},
		childConfigs: [
			{
				name: 'option',
				attributes: {
					value: './value',
					label: './label'
				}
			}
		]
	}
])
	.fetchMock('express:/options/:value', fetchOption)
	.fetchMock('express:/options', fetchOptions);

export const WithOnChange = prejtStory([
	...autocompleteFetchers,
	{
		name: 'autocomplete',
		attributes: {
			label: 'Autocomplete label',
			value: '/value',
			items: '/options',
			onSearch: 'store(/query/search)',
			selected: '/selected/label',
			onChange: 'action(changed)|store(/value)'
		},
		childConfigs: [
			{
				name: 'option',
				attributes: {
					value: './value',
					label: './label'
				}
			}
		]
	}
])
	.fetchMock('express:/options/:value', fetchOption)
	.fetchMock('express:/options', fetchOptions);

export const WithAddButton = prejtStory([
	...autocompleteFetchers,
	{
		name: 'autocomplete',
		attributes: {
			label: 'Autocomplete label',
			value: '/value',
			items: '/options',
			onSearch: 'store(/query/search)',
			selected: '/selected/label',
			onChange: 'action(changed)|store(/value)',
			onAdd: 'action(add)'
		},
		childConfigs: [
			{
				name: 'option',
				attributes: {
					value: './value',
					label: './label'
				}
			}
		]
	}
])
	.fetchMock('express:/options/:value', fetchOption)
	.fetchMock('express:/options', fetchOptions);

export const InModal = prejtStory([
	{
		name: 'dialog',
		attributes: {
			label: 'Dialog with autocomplete',
			onClose: 'action(close)'
		},
		childConfigs: [
			...autocompleteFetchers,
			{
				name: 'autocomplete',
				attributes: {
					label: 'Autocomplete label',
					value: '/value',
					items: '/options',
					onSearch: 'store(/query/search)',
					selected: '/selected/label'
				},
				childConfigs: [
					{
						name: 'option',
						attributes: {
							value: './value',
							label: './label'
						}
					}
				]
			}
		]
	}
])
	.fetchMock('express:/options/:value', fetchOption)
	.fetchMock('express:/options', fetchOptions);

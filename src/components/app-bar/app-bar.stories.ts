import { AppBar } from './app-bar';
import { prejtStory } from '../../utils';

export default {
	title: 'AppBar',
	component: AppBar
};

export const WithButton = prejtStory([
	{
		name: 'app-bar',
		attributes: {},
		childConfigs: [
			{
				name: 'button',
				attributes: {
					label: 'Toolbar button',
					onClick: 'action(click)'
				}
			}
		]
	}
]);

import React from 'react';
import {
	PrejtComponent,
	useStoreDataPath,
	Container,
	getAbsolutePathArray
} from '@prejt/ui-core';
import { Toolbar, AppBar as MaterialAppBar } from '@material-ui/core';

type ToolbarProps = {
	data?: string;
	module?: string;
	variant?: string;
};

export const AppBar: PrejtComponent<ToolbarProps> = ({
	childConfigs = [],
	parentDataPath,
	data = './',
	module,
	eventContext
}) => {
	const dataPath = getAbsolutePathArray(data, parentDataPath);
	const [moduleValue] = useStoreDataPath(module, parentDataPath, eventContext);

	return (
		<>
			<MaterialAppBar position="fixed">
				<Toolbar>
					<Container
						childConfigs={childConfigs}
						dataPath={dataPath}
						module={moduleValue as string}
						eventContext={eventContext}
					/>
				</Toolbar>
			</MaterialAppBar>
			<Toolbar />
		</>
	);
};

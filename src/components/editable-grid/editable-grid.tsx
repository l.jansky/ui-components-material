import React from 'react';
import {
	PrejtComponent,
	ComponentConfig,
	Container,
	useItems,
	useEvents,
	useStoreDataPath,
	useStoreValue,
	useInvokePipe,
	getPipeIO
} from '@prejt/ui-core';
import {
	Table,
	TableBody,
	TableRow,
	TableCell,
	IconButton,
	Icon
} from '@material-ui/core';
import { uuid } from 'uuidv4';
import { withLayout } from '../../hoc';

type GridProps = {
	items: string;
	onNewChange?: string;
	addOnEvent?: string;
	childConfigs: ComponentConfig<unknown>[];
};

const EditableGridComponent: PrejtComponent<GridProps> = ({
	items,
	childConfigs,
	parentDataPath,
	eventContext,
	onNewChange = '',
	addOnEvent
}) => {
	const [componentId] = React.useState(uuid());
	const newItemPath = ['selectors', componentId, 'newItem'];
	const [itemsData, setItemsData] = useStoreDataPath(
		items,
		parentDataPath,
		eventContext
	);
	const [newItemData, setNewItemData] = useStoreValue(newItemPath);
	const itemConfigs = useItems(childConfigs, items, parentDataPath);
	const invokeNewOnChangePipe = useInvokePipe(
		onNewChange,
		parentDataPath,
		eventContext
	);

	React.useEffect(() => {
		if (typeof newItemData !== 'undefined') {
			invokeNewOnChangePipe(getPipeIO(newItemData));
		}
	}, [newItemData]);

	const itemsDataArray = Array.isArray(itemsData) ? itemsData : [];

	useEvents((addOnEvent && addOnEvent.split(',')) || [], eventContext, data => {
		addNewItem(data);
	});

	const addNewItem = (data: any) => {
		if (data) {
			setItemsData([...itemsDataArray, data]);
			setNewItemData(undefined);
		}
	};

	const removeItem = (item: any) => {
		const newItemsData = itemsDataArray.filter((_, key) => item.key !== key);
		setItemsData(newItemsData);
	};

	return (
		<Table>
			<TableBody>
				{itemConfigs.map(row => (
					<TableRow key={row.key}>
						<Container
							childConfigs={row.childConfigs || []}
							dataPath={row.dataPath}
							eventContext={eventContext}
						/>
						<TableCell>
							<IconButton
								color={'secondary'}
								size={'small'}
								onClick={() => removeItem(row)}
							>
								<Icon fontSize={'small'} className={`fa fa-trash`} />
							</IconButton>
						</TableCell>
					</TableRow>
				))}
				<TableRow key={itemConfigs.length}>
					<Container
						childConfigs={childConfigs[0]?.childConfigs || []}
						dataPath={newItemPath}
						eventContext={eventContext}
					/>
					<TableCell>
						{!addOnEvent && (
							<IconButton
								color={'primary'}
								size={'small'}
								onClick={() => addNewItem(newItemData)}
							>
								<Icon fontSize={'small'} className={`fa fa-plus`} />
							</IconButton>
						)}
					</TableCell>
				</TableRow>
			</TableBody>
		</Table>
	);
};

export const EditableGrid = withLayout(EditableGridComponent);

import { EditableGrid } from './editable-grid';
import { prejtStory } from '../../utils';

export default {
	title: 'EditableGrid',
	component: EditableGrid
};

const data = {
	items: ['test1', 'test2', 'test3']
};

export const WithItemsFromDataStore = prejtStory(
	[
		{
			name: 'editable-grid',
			attributes: {
				items: '/items'
			},
			childConfigs: [
				{
					name: 'grid-row',
					attributes: {},
					childConfigs: [
						{
							name: 'grid-col',
							attributes: {},
							childConfigs: [
								{
									name: 'text',
									attributes: {
										value: './'
									}
								}
							]
						}
					]
				}
			]
		}
	],
	data
);

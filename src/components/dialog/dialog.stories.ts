import { Dialog } from './dialog';
import { prejtStory, getTestContent } from '../../utils';

export default {
	title: 'Dialog',
	component: Dialog
};

const dialogContent = getTestContent(30, ['button']);

export const WithContentFromChildren = prejtStory(
	[
		{
			name: 'dialog',
			attributes: {
				label: 'Dialog label',
				visible: '/visible',
				onClose: 'action(close)'
			},
			childConfigs: [
				...dialogContent,
				{
					name: 'button',
					attributes: {
						label: 'Action 1',
						onClick: 'action(action1)'
					}
				},
				{
					name: 'button',
					attributes: {
						label: 'Action 2',
						onClick: 'action(action2)'
					}
				}
			]
		}
	],
	{ visible: true }
);

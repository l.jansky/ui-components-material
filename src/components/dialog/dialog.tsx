import React from 'react';
import {
	PrejtComponent,
	useStoreDataPath,
	Container,
	getAbsolutePathArray,
	useInvokePipe
} from '@prejt/ui-core';
import {
	Dialog as MaterialDialog,
	Slide,
	DialogTitle,
	DialogContent,
	IconButton,
	Icon,
	makeStyles,
	DialogActions,
	useMediaQuery,
	useTheme
} from '@material-ui/core';
import { TransitionProps } from '@material-ui/core/transitions';

type DialogProps = {
	label?: string;
	visible?: string;
	data?: string;
	module?: string;
	onClose?: string;
	full?: string;
};

const Transition = React.forwardRef(function Transition(
	props: TransitionProps & { children?: React.ReactElement<any, any> },
	ref: React.Ref<unknown>
) {
	return <Slide direction="up" ref={ref} {...props} />;
});

const useStyles = makeStyles(theme => ({
	closeButton: {
		position: 'absolute',
		right: theme.spacing(1),
		top: theme.spacing(1),
		color: theme.palette.grey[500]
	},
	dialog: {
		height: '100%'
	}
}));

const CloseButton: React.FC<any> = ({ onClick }) => {
	const classes = useStyles();
	return (
		<IconButton
			aria-label="close"
			className={classes.closeButton}
			onClick={onClick}
		>
			<Icon className={`fa fa-times`} />
		</IconButton>
	);
};

export const Dialog: PrejtComponent<DialogProps> = ({
	childConfigs = [],
	visible,
	data = './',
	parentDataPath,
	module,
	onClose = '',
	eventContext,
	label,
	full
}) => {
	const theme = useTheme();
	const classes = useStyles();
	const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));
	const dataPath = getAbsolutePathArray(data, parentDataPath);
	const [moduleValue] = useStoreDataPath(module, parentDataPath, eventContext);
	const [visibleValue] = useStoreDataPath(
		visible,
		parentDataPath,
		eventContext
	);
	const [labelValue] = useStoreDataPath(label, parentDataPath, eventContext);

	const invokeOnClosePipe = useInvokePipe(
		onClose,
		parentDataPath,
		eventContext
	);

	const actionChildConfigs = childConfigs.filter(
		config => config.name === 'button'
	);

	const nonActionChildConfigs = childConfigs
		.filter(config => config.name !== 'button')
		.map(config =>
			config.name === 'tabs' &&
			(config.attributes as Record<string, unknown>)['variant'] === 'app-bar'
				? {
						...config,
						attributes: {
							...(config.attributes as Record<string, unknown>),
							variant: 'dialog-bar'
						}
				  }
				: config
		);

	return (
		<MaterialDialog
			open={!visible || !!visibleValue}
			onClose={invokeOnClosePipe}
			TransitionComponent={Transition as any}
			fullScreen={fullScreen}
			fullWidth={true}
			maxWidth={'xl'}
			classes={{ paper: full ? classes.dialog : undefined }}
			disableEscapeKeyDown={true}
			disableBackdropClick={true}
			disableEnforceFocus={true}
		>
			{(label || onClose) && (
				<DialogTitle>
					{labelValue}
					{onClose && <CloseButton onClick={invokeOnClosePipe} />}
				</DialogTitle>
			)}
			<DialogContent>
				<Container
					childConfigs={nonActionChildConfigs}
					dataPath={dataPath}
					module={moduleValue as string}
					eventContext={eventContext}
				/>
			</DialogContent>
			<DialogActions>
				<Container
					childConfigs={actionChildConfigs}
					dataPath={dataPath}
					eventContext={eventContext}
				/>
			</DialogActions>
		</MaterialDialog>
	);
};

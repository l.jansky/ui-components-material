import { Drawer } from './drawer';
import { prejtStory } from '../../utils';

export default {
	title: 'Drawer',
	component: Drawer
};

const boxInsideDrawer = {
	name: 'box',
	attributes: {
		label: 'Box inside drawer'
	}
};

export const WithContentFromChildren = prejtStory(
	[
		{
			name: 'drawer',
			attributes: {
				visible: '/visible'
			},
			childConfigs: [boxInsideDrawer]
		}
	],
	{ visible: true }
);

export const OpenedByButton = prejtStory(
	[
		{
			name: 'button',
			attributes: {
				label: 'Open drawer',
				onClick: 'value(1)|store(/visible)'
			}
		},
		{
			name: 'drawer',
			attributes: {
				visible: '/visible',
				onClose: 'value|store(/visible)'
			},
			childConfigs: [boxInsideDrawer]
		}
	],
	{}
);

import React from 'react';
import {
	PrejtComponent,
	useStoreDataPath,
	Container,
	getAbsolutePathArray,
	useInvokePipe
} from '@prejt/ui-core';
import MaterialDrawer from '@material-ui/core/Drawer';
import { makeStyles, Theme } from '@material-ui/core';

type SizeBreakpoints = 'lg' | 'md' | 'sm' | 'xl' | 'xs';
type StyleProps = {
	size: SizeBreakpoints;
};

const useStyles = makeStyles((theme: Theme) => ({
	drawer: ({ size }: StyleProps) => {
		const width = size === 'xs' ? 280 : theme.breakpoints.values[size];
		return {
			width,
			maxWidth: 'calc(100% - 64px)'
		};
	}
}));

type Anchor = 'left' | 'right' | 'top' | 'bottom';

type DrawerProps = {
	visible?: string;
	data?: string;
	module?: string;
	onClose?: string;
	anchor?: Anchor;
	size?: SizeBreakpoints;
};

export const Drawer: PrejtComponent<DrawerProps> = ({
	childConfigs = [],
	visible,
	data = './',
	parentDataPath,
	module,
	onClose = '',
	eventContext,
	anchor = 'right',
	size = 'xs'
}) => {
	const classes = useStyles({ size });
	const dataPath = getAbsolutePathArray(data, parentDataPath);
	const [moduleValue] = useStoreDataPath(module, parentDataPath, eventContext);
	const [visibleValue] = useStoreDataPath(
		visible,
		parentDataPath,
		eventContext
	);
	const invokeOnClosePipe = useInvokePipe(
		onClose,
		parentDataPath,
		eventContext
	);
	return (
		<MaterialDrawer
			anchor={anchor}
			open={!!visibleValue}
			onClose={invokeOnClosePipe}
			classes={{ paper: classes.drawer }}
		>
			<Container
				childConfigs={childConfigs}
				dataPath={dataPath}
				module={moduleValue as string}
				eventContext={eventContext}
			/>
		</MaterialDrawer>
	);
};

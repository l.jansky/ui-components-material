import { Layout } from './layout';
import { prejtStory, getTestContent } from '../../utils';

export default {
	title: 'Layout',
	component: Layout
};

export const BaseLayouts = prejtStory([
	{
		name: 'layout',
		attributes: {},
		childConfigs: [
			{
				name: 'box',
				attributes: {
					label: 'Left box',
					xs: '4'
				},
				childConfigs: [
					{
						name: 'text',
						attributes: {
							label: 'Test 1'
						}
					},
					{
						name: 'text',
						attributes: {
							label: 'Test 2'
						}
					}
				]
			},
			{
				name: 'box',
				attributes: {
					label: 'Right box',
					xs: '8'
				},
				childConfigs: [
					{
						name: 'text',
						attributes: {
							label: 'Test 2'
						}
					},
					{
						name: 'text',
						attributes: {
							label: 'Test 3'
						}
					}
				]
			},
			{
				name: 'box',
				attributes: {
					label: 'Bottom box',
					xs: '9'
				},
				childConfigs: [
					{
						name: 'layout',
						attributes: {},
						childConfigs: [
							{
								name: 'text',
								attributes: {
									label: 'Test 3',
									xs: '3'
								}
							},
							{
								name: 'text',
								attributes: {
									label: 'Test 4',
									xs: '5'
								}
							},
							{
								name: 'text',
								attributes: {
									label: 'Test 5',
									xs: '4'
								}
							},
							{
								name: 'text',
								attributes: {
									label: 'Test 6'
								}
							}
						]
					}
				]
			}
		]
	}
]);

const testComponents = getTestContent(20);

export const WithComponents = prejtStory([
	{
		name: 'layout',
		attributes: {},
		childConfigs: testComponents.map(component => ({
			...component,
			attributes: {
				...component.attributes,
				xs: '4'
			}
		}))
	}
]);

import React from 'react';
import {
	Container,
	PrejtComponent,
	useStoreDataPath,
	getAbsolutePathArray
} from '@prejt/ui-core';
import Grid, {
	GridDirection,
	GridJustification,
	GridItemsAlignment
} from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core';
import { withLayout } from '../../hoc';

type FixedPositions = 'bottom' | 'bottom-left' | 'bottom-right';

type LayoutProps = {
	data?: string;
	module?: string;
	direction?: GridDirection;
	justify?: GridJustification;
	alignItems?: GridItemsAlignment;
	spacing?: string;
	fixed?: FixedPositions;
};

type StyleProps = {
	fixed?: FixedPositions;
};

const useStyles = makeStyles(theme => ({
	root: ({ fixed }: StyleProps) => {
		return fixed
			? {
					position: 'fixed',
					bottom: theme.spacing(2),
					right: theme.spacing(2)
			  }
			: {};
	}
}));

const LayoutComponent: PrejtComponent<LayoutProps> = ({
	childConfigs = [],
	data = './',
	module,
	direction,
	justify,
	alignItems,
	parentDataPath,
	eventContext,
	spacing,
	fixed
}) => {
	const classes = useStyles({ fixed });
	const dataPath = getAbsolutePathArray(data, parentDataPath);
	const [moduleValue] = useStoreDataPath(module, parentDataPath, eventContext);

	const spacingValue = parseInt(spacing || '0');

	return (
		<div className={classes.root}>
			<Grid
				container={true}
				spacing={spacingValue as any}
				direction={direction}
				justify={justify}
				alignItems={alignItems}
			>
				<Container
					childConfigs={childConfigs}
					dataPath={dataPath}
					module={moduleValue as string}
					eventContext={eventContext}
				/>
			</Grid>
		</div>
	);
};

export const Layout = withLayout(LayoutComponent);

import { Switch } from './switch';
import { prejtStory } from '../../utils';

export default {
	title: 'Switch',
	component: Switch
};

export const WithoutOnChange = prejtStory(
	[
		{
			name: 'switch',
			attributes: {
				label: 'Without onChange',
				value: '/value'
			}
		}
	],
	{
		value: false
	}
);

export const WithOnChange = prejtStory(
	[
		{
			name: 'switch',
			attributes: {
				label: 'With onChange',
				value: '/value',
				onChange: 'action(changed)|store(/value)'
			}
		}
	],
	{
		value: false
	}
);

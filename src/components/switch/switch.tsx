import React from 'react';
import {
	PrejtComponent,
	useStoreDataPath,
	useInvokePipe,
	getPipeIO
} from '@prejt/ui-core';
import { Switch as MaterialSwitch, FormControlLabel } from '@material-ui/core';
import { withLayout } from '../../hoc';

type SwitchProps = {
	value: string;
	label?: string;
	onChange?: string;
};

const SwitchComponent: PrejtComponent<SwitchProps> = ({
	value,
	label,
	onChange = '',
	parentDataPath,
	eventContext
}) => {
	const [dataValue, setDataValue] = useStoreDataPath(
		value,
		parentDataPath,
		eventContext
	);

	const [labelValue] = useStoreDataPath(label, parentDataPath, eventContext);

	const invokeOnChangePath = useInvokePipe(
		onChange,
		parentDataPath,
		eventContext
	);

	const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
		if (onChange) {
			invokeOnChangePath(getPipeIO(event.target.checked));
		} else {
			setDataValue(event.target.checked ? 1 : 0);
		}
	};

	return (
		<FormControlLabel
			control={
				<MaterialSwitch
					checked={!!dataValue}
					onChange={handleChange}
					color="primary"
				/>
			}
			label={labelValue}
		/>
	);
};

export const Switch = withLayout(SwitchComponent);

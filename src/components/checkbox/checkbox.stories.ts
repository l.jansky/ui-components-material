import { Checkbox } from './checkbox';
import { prejtStory } from '../../utils';

export default {
	title: 'Checkbox',
	component: Checkbox
};

export const WithoutOnChange = prejtStory(
	[
		{
			name: 'checkbox',
			attributes: {
				label: '/label',
				value: '/value'
			}
		}
	],
	{
		label: 'Checkbox label',
		value: '1'
	}
);

export const WithOnChange = prejtStory(
	[
		{
			name: 'checkbox',
			attributes: {
				label: '/label',
				value: '/value',
				onChange: 'action(changed)|store(/value)'
			}
		}
	],
	{
		label: 'Checkbox label',
		value: '1'
	}
);

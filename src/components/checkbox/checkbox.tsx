import React from 'react';
import {
	PrejtComponent,
	useStoreDataPath,
	useInvokePipe,
	getPipeIO
} from '@prejt/ui-core';
import {
	FormControlLabel,
	Checkbox as MaterialCheckbox
} from '@material-ui/core';
import { withLayout } from '../../hoc';

type CheckboxProps = {
	label: string;
	value: string;
	onChange?: string;
};

const CheckboxComponent: PrejtComponent<CheckboxProps> = ({
	label,
	parentDataPath,
	value,
	eventContext,
	onChange = ''
}) => {
	const [labelValue] = useStoreDataPath(label, parentDataPath, eventContext);
	const [dataValue, setDataValue] = useStoreDataPath(
		value,
		parentDataPath,
		eventContext
	);
	const invokeOnChangePath = useInvokePipe(
		onChange,
		parentDataPath,
		eventContext
	);

	const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
		if (onChange) {
			invokeOnChangePath(getPipeIO(event.target.checked));
		} else {
			setDataValue(event.target.checked ? 1 : 0);
		}
	};

	return (
		<FormControlLabel
			control={
				<MaterialCheckbox checked={!!dataValue} onChange={handleChange} />
			}
			label={labelValue}
		/>
	);
};

export const Checkbox = withLayout(CheckboxComponent);

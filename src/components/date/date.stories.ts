import { DateComponent } from './date';
import { prejtStory } from '../../utils';

export default {
	title: 'Date',
	component: DateComponent
};

export const WithoutOnChange = prejtStory(
	[
		{
			name: 'date',
			attributes: {
				label: '/label',
				value: '/value'
			}
		}
	],
	{
		label: 'Date label',
		value: '2020-01-25'
	}
);

export const WithOnChange = prejtStory(
	[
		{
			name: 'date',
			attributes: {
				label: '/label',
				value: '/value',
				onChange: 'action(changed)|store(/value)'
			}
		}
	],
	{
		label: 'Date label',
		value: '2020-01-25'
	}
);

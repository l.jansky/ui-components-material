import React from 'react';
import {
	PrejtComponent,
	useStoreDataPath,
	useInvokePipe,
	getPipeIO
} from '@prejt/ui-core';
import { DatePicker } from '@material-ui/pickers';
import moment from 'moment';
import { withLayout } from '../../hoc';

type DateProps = {
	label: string;
	value: string;
	onChange?: string;
};

const DateComponentBase: PrejtComponent<DateProps> = ({
	label,
	parentDataPath,
	value,
	onChange = '',
	eventContext
}) => {
	const [labelValue] = useStoreDataPath(label, parentDataPath, eventContext);
	const [dataValue, setDataValue] = useStoreDataPath(
		value,
		parentDataPath,
		eventContext
	);
	const invokeOnChangePath = useInvokePipe(
		onChange,
		parentDataPath,
		eventContext
	);

	const handleChange = (date: moment.Moment | null) => {
		const dateString = date ? date.format('YYYY-MM-DD') : '';
		if (onChange) {
			invokeOnChangePath(getPipeIO(dateString));
		} else {
			setDataValue(dateString);
		}
	};

	const dateValue = typeof dataValue === 'string' ? moment(dataValue) : null;

	return (
		<DatePicker
			label={labelValue}
			value={dateValue}
			onChange={handleChange}
			fullWidth={true}
			margin={'normal'}
		/>
	);
};

export const DateComponent = withLayout(DateComponentBase, true);

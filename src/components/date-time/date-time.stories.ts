import { DateTime } from './date-time';
import { prejtStory } from '../../utils';

export default {
	title: 'DateTime',
	component: DateTime
};

export const WithoutOnChange = prejtStory(
	[
		{
			name: 'date-time',
			attributes: {
				label: '/label',
				value: '/value'
			}
		}
	],
	{
		label: 'DateTime label',
		value: '2020-01-25 08:50:00'
	}
);

export const WithOnChange = prejtStory(
	[
		{
			name: 'date-time',
			attributes: {
				label: '/label',
				value: '/value',
				onChange: 'action(changed)|store(/value)'
			}
		}
	],
	{
		label: 'DateTime label',
		value: '2020-01-25 08:50:00'
	}
);

import React from 'react';
import {
	PrejtComponent,
	useStoreDataPath,
	useInvokePipe,
	getPipeIO
} from '@prejt/ui-core';
import { DateTimePicker } from '@material-ui/pickers';
import moment from 'moment';
import { withLayout } from '../../hoc';

type DateTimeProps = {
	label: string;
	value: string;
	onChange?: string;
};

const DateTimeComponent: PrejtComponent<DateTimeProps> = ({
	label,
	parentDataPath,
	value,
	onChange = '',
	eventContext
}) => {
	const [labelValue] = useStoreDataPath(label, parentDataPath, eventContext);
	const [dataValue, setDataValue] = useStoreDataPath(
		value,
		parentDataPath,
		eventContext
	);
	const invokeOnChangePath = useInvokePipe(
		onChange,
		parentDataPath,
		eventContext
	);

	const handleChange = (date: moment.Moment | null) => {
		const dateString = date ? date.format('YYYY-MM-DD HH:mm:ss') : '';
		if (onChange) {
			invokeOnChangePath(getPipeIO(dateString));
		} else {
			setDataValue(dateString);
		}
	};

	const dateValue = typeof dataValue === 'string' ? moment(dataValue) : null;

	return (
		<DateTimePicker
			ampm={false}
			label={labelValue}
			value={dateValue}
			onChange={handleChange}
			fullWidth={true}
			margin={'normal'}
		/>
	);
};

export const DateTime = withLayout(DateTimeComponent, true);

import React from 'react';
import { MuiPickersUtilsProvider } from '@material-ui/pickers';
import MomentUtils from '@date-io/moment';
import moment from 'moment';

import 'moment/locale/cs';

moment.locale('cs');

export const MuiComponentsProvider: React.FC = ({ children }) => {
	return (
		<MuiPickersUtilsProvider utils={MomentUtils}>
			{children}
		</MuiPickersUtilsProvider>
	);
};

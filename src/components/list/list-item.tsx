import React from 'react';
import { PrejtComponent, Container, useInvokePipe } from '@prejt/ui-core';
import {
	ListItem as MaterialListItem,
	Grid,
	GridDirection,
	GridJustification,
	GridItemsAlignment
} from '@material-ui/core';

type ListItemProps = {
	onClick?: string;
	direction?: GridDirection;
	justify?: GridJustification;
	alignItems?: GridItemsAlignment;
	spacing?: number;
};

export const ListItem: PrejtComponent<ListItemProps> = React.forwardRef(
	(
		{
			parentDataPath,
			eventContext,
			childConfigs,
			onClick = '',
			direction,
			justify,
			alignItems,
			spacing
		},
		ref
	) => {
		const invokeOnClickPipe = useInvokePipe(
			onClick,
			parentDataPath,
			eventContext
		);

		const listItemProps = onClick
			? {
					onClick: (e: any) => {
						e.preventDefault();
						invokeOnClickPipe({ value: undefined });
					},
					button: true
			  }
			: {};

		return (
			<MaterialListItem {...(listItemProps as any)}>
				<Grid
					container={true}
					spacing={spacing as any}
					direction={direction}
					justify={justify}
					alignItems={alignItems}
					ref={ref as any}
				>
					<Container
						childConfigs={childConfigs || []}
						dataPath={parentDataPath}
						eventContext={eventContext}
					/>
				</Grid>
			</MaterialListItem>
		);
	}
);

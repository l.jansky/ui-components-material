import React from 'react';
import {
	PrejtComponent,
	ComponentConfig,
	useItems,
	useEvents,
	useInvokePipe
} from '@prejt/ui-core';
import { List as MaterialList, Divider } from '@material-ui/core';
import { ListItem } from './list-item';
import { withLayout } from '../../hoc';

type ListProps = {
	items: string;
	childConfigs: ComponentConfig<unknown>[];
	disableSeparator?: string;
	focusLastItemOnEvent?: string;
	onLastItem?: string;
};

const ListComponent: PrejtComponent<ListProps> = ({
	items,
	childConfigs,
	parentDataPath,
	eventContext,
	disableSeparator,
	focusLastItemOnEvent,
	onLastItem
}) => {
	const itemConfigs = useItems(childConfigs, items, parentDataPath);
	const storedLastItemRef = React.useRef<HTMLInputElement>();

	useEvents(
		(focusLastItemOnEvent && focusLastItemOnEvent.split(',')) || [],
		eventContext,
		data => {
			setTimeout(() => {
				const inputs = storedLastItemRef.current?.getElementsByTagName('input');
				if (inputs && inputs[0]) {
					inputs[0].focus();
				}
			}, 0);
		}
	);

	const invokeOnLastItemPipe = useInvokePipe(
		onLastItem || '',
		parentDataPath,
		eventContext
	);

	const observer = React.useRef<IntersectionObserver>();

	const lastItemRef = React.useCallback(node => {
		storedLastItemRef.current = node;
		if (observer.current) observer.current.disconnect();
		observer.current = new IntersectionObserver(entries => {
			if (entries[0].isIntersecting) {
				onLastItem && invokeOnLastItemPipe({ value: undefined });
			}
		});
		if (node) observer.current.observe(node);
	}, []);

	const disableSeparatorValue = disableSeparator === 'true';
	return (
		<MaterialList>
			{itemConfigs.map(({ dataPath, childConfigs, key, attributes }, index) => (
				<React.Fragment key={key}>
					<ListItem
						parentDataPath={dataPath}
						childConfigs={childConfigs || []}
						eventContext={eventContext}
						ref={index === itemConfigs.length - 1 ? lastItemRef : undefined}
						{...attributes}
					/>
					{!disableSeparatorValue && <Divider component="li" />}
				</React.Fragment>
			))}
		</MaterialList>
	);
};

export const List = withLayout(ListComponent);

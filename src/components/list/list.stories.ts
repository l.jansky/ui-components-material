import { List } from './list';
import { prejtStory, getItems } from '../../utils';
import { ComponentConfig } from '@prejt/ui-core';

export default {
	title: 'List',
	component: List
};

const items = getItems(5, { label: 'List item' });

let counter = 0;

const fetchItems = () => getItems(10, { label: `List item ${counter++}` });

const listWithFetcher: ComponentConfig[] = [
	{
		name: 'fetcher',
		attributes: {
			fetch: '//items',
			onData: 'concatTo(/items)|store(/items)',
			query: '/query',
			autofetch: 'true'
		}
	},
	{
		name: 'list',
		attributes: {
			onLastItem: 'value(/query/page)|inc|store(/query/page)',
			items: '/items'
		},
		childConfigs: [
			{
				name: 'list-item',
				attributes: {
					onClick: 'action'
				},
				childConfigs: [
					{
						name: 'typography',
						attributes: {
							value: 'Main title',
							variant: 'body1',
							xs: '9'
						}
					},
					{
						name: 'typography',
						attributes: {
							value: './label',
							variant: 'body2',
							xs: '6'
						}
					}
				]
			}
		]
	}
];

const initialState = {
	items: [],
	query: {
		page: 0
	}
};

export const WithFocusingLastInput = prejtStory([
	{
		name: 'button',
		attributes: {
			label: 'Focus last input',
			onClick: 'emit(focus)'
		}
	},
	{
		name: 'list',
		attributes: {
			focusLastItemOnEvent: 'focus'
		},
		childConfigs: items.map<ComponentConfig>(({ label }) => ({
			name: 'list-item',
			attributes: {
				onClick: 'action'
			},
			childConfigs: [
				{
					name: 'text',
					attributes: {
						label: 'Test',
						value: './text-value',
						xs: '12'
					}
				},
				{
					name: 'typography',
					attributes: {
						value: 'Main title',
						variant: 'body1',
						xs: '9'
					}
				},
				{
					name: 'typography',
					attributes: {
						value: label as string,
						variant: 'body2',
						xs: '6'
					}
				}
			] as ComponentConfig[]
		}))
	}
]);

export const WithInfiniteItems = prejtStory(
	listWithFetcher,
	initialState
).fetchMock('express:/items', fetchItems);

export const WithInfiniteItemsInDialog = prejtStory(
	[
		{
			name: 'dialog',
			attributes: {
				label: 'Dialog label',
				visible: '/visible',
				onClose: 'action(close)'
			},
			childConfigs: listWithFetcher
		}
	],
	{
		...initialState,
		visible: true
	}
).fetchMock('express:/items', fetchItems);

export const WithInfiniteItemsInTab = prejtStory(
	[
		{
			name: 'tabs',
			attributes: {
				label: 'Test tabs',
				variant: 'app-bar'
			},
			childConfigs: [
				{
					name: 'tab',
					attributes: {
						label: `Tab 1`
					},
					childConfigs: listWithFetcher
				}
			]
		}
	],
	initialState
).fetchMock('express:/items', fetchItems);

export const WithInfiniteItemsInModalTab = prejtStory(
	[
		{
			name: 'dialog',
			attributes: {
				label: 'Dialog label',
				visible: '/visible',
				onClose: 'action(close)'
			},
			childConfigs: [
				{
					name: 'tabs',
					attributes: {
						label: 'Test tabs',
						variant: 'app-bar'
					},
					childConfigs: [
						{
							name: 'tab',
							attributes: {
								label: `Tab 1`
							},
							childConfigs: listWithFetcher
						}
					]
				}
			]
		}
	],
	{
		...initialState,
		visible: true
	}
).fetchMock('express:/items', fetchItems);

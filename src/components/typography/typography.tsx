import React from 'react';
import { PrejtComponent, useStoreDataPath } from '@prejt/ui-core';
import {
	Typography as MaterialTypography,
	TypographyVariant
} from '@material-ui/core';
import { withLayout } from '../../hoc/with-layout/with-layout';

type TypographyProps = {
	value: string;
	variant?: TypographyVariant;
};

export const TypographyComponent: PrejtComponent<TypographyProps> = ({
	value,
	variant,
	parentDataPath,
	eventContext
}) => {
	const [dataValue] = useStoreDataPath(value, parentDataPath, eventContext);
	return <MaterialTypography variant={variant}>{dataValue}</MaterialTypography>;
};

export const Typography = withLayout(TypographyComponent);

import { Typography } from './typography';
import { prejtStory } from '../../utils';

export default {
	title: 'Typography',
	component: Typography
};

const variants = [
	'h1',
	'h2',
	'h3',
	'h4',
	'h5',
	'h6',
	'subtitle1',
	'subtitle2',
	'body1',
	'body2',
	'caption',
	'button',
	'overline'
];

export const BaseRender = prejtStory([
	{
		name: 'typography',
		attributes: {
			value: 'Without variant'
		}
	},
	...variants.map(variant => ({
		name: 'typography',
		attributes: {
			value: `Variant ${variant}`,
			variant
		}
	}))
]);

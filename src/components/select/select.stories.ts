import { Select } from './select';
import { prejtStory, getItems } from '../../utils';

export default {
	title: 'Select',
	component: Select
};

const items = getItems(5, { value: '', label: 'Option' });

export const WithoutOnChange = prejtStory([
	{
		name: 'select',
		attributes: {
			value: '/value'
		},
		childConfigs: items.map<any>(item => ({
			name: 'option',
			attributes: item
		}))
	}
]);

export const WithOnChange = prejtStory([
	{
		name: 'select',
		attributes: {
			value: '/value',
			onChange: 'action(changed)|store(/value)'
		},
		childConfigs: items.map<any>(item => ({
			name: 'option',
			attributes: item
		}))
	}
]);

export const WithValuesFromData = prejtStory(
	[
		{
			name: 'select',
			attributes: {
				value: '/value',
				items: '/items'
			},
			childConfigs: [
				{
					name: 'option',
					attributes: {
						value: './value',
						label: './label'
					}
				}
			]
		}
	],
	{ items }
);

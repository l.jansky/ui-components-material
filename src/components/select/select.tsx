import React from 'react';
import {
	PrejtComponent,
	useStoreDataPath,
	ComponentConfig,
	Items,
	useInvokePipe,
	getPipeIO
} from '@prejt/ui-core';
import { NativeSelect, FormControl, InputLabel } from '@material-ui/core';
import { withLayout } from '../../hoc';

type SelectProps = {
	label: string;
	value: string;
	items?: string;
	onChange?: string;
	childConfigs: ComponentConfig<unknown>[];
};

const SelectComponent: PrejtComponent<SelectProps> = ({
	label,
	parentDataPath,
	value,
	onChange = '',
	items,
	childConfigs,
	eventContext
}) => {
	const [labelValue] = useStoreDataPath(label, parentDataPath, eventContext);
	const [dataValue, setDataValue] = useStoreDataPath(
		value,
		parentDataPath,
		eventContext
	);
	const invokeOnChangePath = useInvokePipe(
		onChange,
		parentDataPath,
		eventContext
	);

	const handleChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
		if (onChange) {
			invokeOnChangePath(getPipeIO(event.target.value));
		} else {
			setDataValue(event.target.value);
		}
	};

	return (
		<FormControl fullWidth={true} margin={'normal'}>
			<InputLabel>{labelValue}</InputLabel>
			<NativeSelect value={dataValue || ''} onChange={handleChange}>
				<Items
					dataPath={parentDataPath}
					items={items}
					childConfigs={childConfigs}
					eventContext={eventContext}
				/>
			</NativeSelect>
		</FormControl>
	);
};

export const Select = withLayout(SelectComponent, true);

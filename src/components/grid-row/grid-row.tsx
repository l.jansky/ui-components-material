import React from 'react';
import {
	PrejtComponent,
	useStoreDataPath,
	Container,
	getAbsolutePathArray
} from '@prejt/ui-core';
import { TableRow } from '@material-ui/core';

type GridRowProps = {
	data?: string;
	module?: string;
};

export const GridRow: PrejtComponent<GridRowProps> = ({
	childConfigs = [],
	parentDataPath,
	data = './',
	module,
	eventContext
}) => {
	const dataPath = getAbsolutePathArray(data, parentDataPath);
	const [moduleValue] = useStoreDataPath(module, parentDataPath, eventContext);

	return (
		<TableRow>
			<Container
				childConfigs={childConfigs}
				dataPath={dataPath}
				module={moduleValue as string}
				eventContext={eventContext}
			/>
		</TableRow>
	);
};

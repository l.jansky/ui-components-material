import React from 'react';
import { Tab as MaterialTab } from '@material-ui/core';
import {
	PathSegment,
	PrejtContext,
	useStoreDataPath,
	withEnabled
} from '@prejt/ui-core';

type TabProps = {
	label: string;
	enabled?: string;
	parentDataPath: PathSegment[];
	eventContext: PrejtContext;
};

const TabComponent: React.FC<TabProps> = ({
	label,
	parentDataPath,
	eventContext,
	...props
}) => {
	const [labelValue] = useStoreDataPath(label, parentDataPath, eventContext);

	return <MaterialTab label={labelValue} {...props} />;
};

export const Tab = withEnabled(TabComponent);

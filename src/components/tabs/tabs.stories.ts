import { Tabs } from './tabs';
import { prejtStory, createArray, getTestContent } from '../../utils';
import { ComponentConfig } from '@prejt/ui-core';

export default {
	title: 'Tabs',
	component: Tabs
};

const tabs = createArray(3, i => ({
	name: 'tab',
	attributes: {
		label: `Tab ${i}`
	},
	childConfigs: [...getTestContent(5, ['box', 'grid'])]
}));

const header: ComponentConfig = {
	name: 'tabs-header',
	attributes: {},
	childConfigs: [
		{
			name: 'button',
			attributes: {
				label: 'Menu',
				variant: 'icon',
				icon: 'bars',
				color: 'inherit',
				onClick: 'action(menu)'
			}
		},
		{
			name: 'typography',
			attributes: {
				value: 'Test label'
			}
		},
		{
			name: 'button',
			attributes: {
				label: 'Close',
				variant: 'icon',
				icon: 'close',
				color: 'inherit',
				onClick: 'action(close)'
			}
		}
	]
};

tabs.push({
	name: 'button',
	attributes: {
		label: 'Close',
		variant: 'icon',
		icon: 'close',
		color: 'inherit',
		onClick: 'action(close)'
	}
});

export const BaseRender = prejtStory([
	{
		name: 'tabs',
		attributes: {},
		childConfigs: tabs
	}
]);

export const InsideModalWithoutHeader = prejtStory([
	{
		name: 'dialog',
		attributes: {
			label: 'Test dialog',
			visible: '1'
		},
		childConfigs: [
			{
				name: 'tabs',
				attributes: {},
				childConfigs: tabs
			}
		]
	}
]);

export const InsideModalWithHeader = prejtStory([
	{
		name: 'dialog',
		attributes: {
			label: 'Test dialog',
			visible: '1'
		},
		childConfigs: [
			{
				name: 'tabs',
				attributes: {},
				childConfigs: [...tabs, header]
			}
		]
	}
]);

export const InsideModalAppBarWithHeader = prejtStory([
	{
		name: 'dialog',
		attributes: {
			label: 'Test dialog',
			visible: '1'
		},
		childConfigs: [
			{
				name: 'tabs',
				attributes: {
					variant: 'app-bar'
				},
				childConfigs: [...tabs, header]
			}
		]
	}
]);

export const InsideModalAppBarWithoutHeader = prejtStory([
	{
		name: 'dialog',
		attributes: {
			label: 'Test dialog',
			visible: '1'
		},
		childConfigs: [
			{
				name: 'tabs',
				attributes: {
					variant: 'app-bar'
				},
				childConfigs: tabs
			}
		]
	}
]);

export const AsPageAppBarWithoutHeader = prejtStory([
	{
		name: 'tabs',
		attributes: {
			variant: 'app-bar'
		},
		childConfigs: tabs
	}
]);

export const AsPageAppBarWithHeader = prejtStory([
	{
		name: 'tabs',
		attributes: {
			variant: 'app-bar'
		},
		childConfigs: [...tabs, header]
	}
]);

export const InsideBoxWithoutLabel = prejtStory([
	{
		name: 'box',
		attributes: {
			label: 'Box label'
		},
		childConfigs: [
			{
				name: 'tabs',
				attributes: {},
				childConfigs: tabs
			}
		]
	}
]);

export const InsideBoxWithLabel = prejtStory([
	{
		name: 'box',
		attributes: {
			label: 'Box label'
		},
		childConfigs: [
			{
				name: 'tabs',
				attributes: {
					label: 'Tabs label'
				},
				childConfigs: tabs
			}
		]
	}
]);

BaseRender.story;

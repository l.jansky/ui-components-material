import React from 'react';
import { PrejtComponent, Container, ComponentConfig } from '@prejt/ui-core';
import { AppBar, Tabs as MaterialTabs, Toolbar } from '@material-ui/core';
import SwipeableViews from 'react-swipeable-views';
import { Tab } from './tab';
import { withLayout } from '../../hoc';

type TabsProps = {
	childConfigs: ComponentConfig[];
	variant?: string;
};

export const TabsComponent: PrejtComponent<TabsProps> = ({
	childConfigs,
	parentDataPath,
	eventContext,
	variant
}) => {
	const [selected, setSelected] = React.useState(0);
	const tabs = childConfigs ? childConfigs.filter(it => it.name === 'tab') : [];
	const header = childConfigs.find(it => it.name === 'tabs-header');

	const tabsHeader = (
		<>
			{header && (
				<Toolbar>
					<Container
						childConfigs={header.childConfigs || []}
						dataPath={parentDataPath}
						eventContext={eventContext}
					/>
				</Toolbar>
			)}
			<MaterialTabs
				value={selected}
				onChange={(_, selected) => setSelected(selected)}
			>
				{tabs.map((tab, index) => (
					<Tab
						key={index}
						label={(tab.attributes as any).label}
						enabled={(tab.attributes as any).enabled}
						parentDataPath={parentDataPath}
						eventContext={eventContext}
						{...tab.attributes}
					/>
				))}
			</MaterialTabs>
		</>
	);

	const lineToolbar = header ? <Toolbar /> : null;

	return (
		<>
			{variant === 'app-bar' || variant === 'dialog-bar' ? (
				<>
					<AppBar position={variant === 'app-bar' ? 'fixed' : 'absolute'}>
						{tabsHeader}
					</AppBar>
					{lineToolbar}
				</>
			) : (
				tabsHeader
			)}
			<SwipeableViews axis="x" index={selected} onChangeIndex={setSelected}>
				{tabs.map((tab, index) => (
					<Container
						key={index}
						childConfigs={tab.childConfigs || []}
						dataPath={parentDataPath}
						eventContext={eventContext}
					/>
				))}
			</SwipeableViews>
		</>
	);
};

export const Tabs = withLayout(TabsComponent);

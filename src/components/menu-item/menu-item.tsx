import React from 'react';
import {
	PrejtComponent,
	useStoreDataPath,
	useInvokePipe
} from '@prejt/ui-core';
import { ListItem, ListItemText, ListItemIcon, Icon } from '@material-ui/core';

type MenuItemProps = {
	label: string;
	onClick: string;
	icon?: string;
};

export const MenuItem: PrejtComponent<MenuItemProps> = ({
	onClick,
	parentDataPath,
	label,
	icon,
	eventContext
}) => {
	const [labelValue] = useStoreDataPath(label, parentDataPath, eventContext);
	const [iconValue] = useStoreDataPath(icon, parentDataPath, eventContext);
	const invokeOnClickPipe = useInvokePipe(
		onClick,
		parentDataPath,
		eventContext
	);

	const handleClick = (e: any) => {
		e.preventDefault();
		invokeOnClickPipe({ value: undefined });
	};

	const iconComponent = iconValue ? (
		<ListItemIcon>
			<Icon className={`fa fa-${iconValue}`} />
		</ListItemIcon>
	) : null;

	return (
		<ListItem button>
			{iconComponent}
			<ListItemText primary={labelValue} onClick={handleClick} />
		</ListItem>
	);
};

import { ComponentConfig } from '@prejt/ui-core';
import { Button } from './button';
import { prejtStory } from '../../utils';

export default {
	title: 'Button',
	component: Button
};

const onClick = 'action(clicked)';

export const WithLabelFromStore = prejtStory(
	[
		{
			name: 'button',
			attributes: {
				label: '/label',
				onClick
			}
		}
	],
	{ label: 'Default button' }
);

const variants = ['text', 'outlined', 'contained', 'icon', 'floating'];
const colors = [undefined, 'primary', 'secondary'];
const sizes = ['small', 'medium', 'large'];

const moduleArray: ComponentConfig[] = [];
for (const size of sizes) {
	for (const color of colors) {
		const box: ComponentConfig = {
			name: 'box',
			attributes: {},
			childConfigs: []
		};

		for (const variant of variants) {
			if (variant !== 'icon' && variant !== 'floating') {
				box.childConfigs!.push({
					name: 'button',
					attributes: {
						label: variant,
						variant,
						color: color as string,
						size,
						onClick
					}
				});
			}
		}

		for (const variant of variants) {
			box.childConfigs!.push({
				name: 'button',
				attributes: {
					label: variant,
					variant,
					color: color as string,
					size,
					onClick,
					icon: 'delete'
				}
			});
		}

		moduleArray.push(box);
	}
}

export const Variants = prejtStory(moduleArray);

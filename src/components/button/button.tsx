import React from 'react';
import {
	PrejtComponent,
	useStoreDataPath,
	useInvokePipe
} from '@prejt/ui-core';
import {
	Button as MaterialButton,
	makeStyles,
	IconButton,
	Icon,
	Tooltip,
	Fab
} from '@material-ui/core';
import { withLayout } from '../../hoc';

type Color = 'inherit' | 'primary' | 'secondary' | 'default';
type Variant = 'text' | 'outlined' | 'contained' | 'icon' | 'floating';
type Size = 'small' | 'medium' | 'large';

type ButtonProps = {
	label: string;
	icon?: string;
	variant?: Variant;
	color?: Color;
	size?: Size;
	onClick?: string;
};

const useStyles = makeStyles(theme => ({
	root: {
		margin: theme.spacing(1)
	}
}));

const ButtonComponent: PrejtComponent<ButtonProps> = ({
	label,
	icon,
	variant,
	color,
	size,
	parentDataPath,
	onClick,
	eventContext
}) => {
	const [labelValue] = useStoreDataPath(label, parentDataPath, eventContext);

	const invokeOnClickPipe = useInvokePipe(
		onClick || '',
		parentDataPath,
		eventContext
	);
	const classes = useStyles();

	const iconFontSize = size === 'medium' ? undefined : size;
	const iconComponent = icon ? (
		<Icon fontSize={iconFontSize} className={`fa fa-${icon}`} />
	) : null;

	const onClickHandler = () =>
		onClick && invokeOnClickPipe({ value: undefined });

	if (variant === 'icon') {
		const iconSize = size === 'large' ? undefined : size;
		return (
			<Tooltip title={labelValue as string}>
				<IconButton
					color={color}
					size={iconSize}
					onClick={onClickHandler}
					className={classes.root}
				>
					{iconComponent}
				</IconButton>
			</Tooltip>
		);
	} else if (variant === 'floating') {
		const iconSize = size === 'large' ? undefined : size;
		return (
			<Tooltip title={labelValue as string}>
				<Fab
					color={color}
					size={iconSize}
					onClick={onClickHandler}
					className={classes.root}
				>
					{iconComponent || <Icon />}
				</Fab>
			</Tooltip>
		);
	} else {
		return (
			<MaterialButton
				className={classes.root}
				variant={variant || 'contained'}
				color={color}
				size={size}
				onClick={onClickHandler}
				startIcon={iconComponent}
			>
				{(labelValue as string) || ''}
			</MaterialButton>
		);
	}
};

export const Button = withLayout(ButtonComponent);

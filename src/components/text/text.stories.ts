import { Text } from './text';
import { prejtStory } from '../../utils';

export default {
	title: 'Text',
	component: Text
};

export const WithoutOnChange = prejtStory(
	[
		{
			name: 'text',
			attributes: {
				label: 'Without onChange',
				value: '/value'
			}
		}
	],
	{
		value: 'Text value'
	}
);

export const WithOnChange = prejtStory(
	[
		{
			name: 'text',
			attributes: {
				label: 'With onChange',
				value: '/value',
				onChange: 'action(changed)|store(/value)'
			}
		}
	],
	{
		value: 'Text value'
	}
);

import React from 'react';
import {
	PrejtComponent,
	useStoreDataPath,
	useInputStoreDataPath,
	useInvokePipe,
	getPipeIO
} from '@prejt/ui-core';
import { TextField, Size } from '@material-ui/core';
import { withLayout } from '../../hoc/with-layout/with-layout';

type TextProps = {
	label: string;
	value: string;
	onChange?: string;
	type?: string;
	size?: Size;
};

export const TextComponent: PrejtComponent<TextProps> = ({
	label,
	parentDataPath,
	value,
	onChange = '',
	type,
	eventContext,
	size
}) => {
	const [labelValue] = useStoreDataPath(label, parentDataPath, eventContext);
	const [dataValue, setDataValue] = useInputStoreDataPath(
		value,
		parentDataPath
	);

	const invokeOnChangePath = useInvokePipe(
		onChange,
		parentDataPath,
		eventContext
	);

	const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
		if (onChange) {
			invokeOnChangePath(getPipeIO(event.target.value));
		} else {
			setDataValue(event.target.value);
		}
	};

	return (
		<TextField
			label={labelValue}
			value={dataValue || ''}
			onChange={handleChange}
			margin={size === 'small' ? 'none' : 'normal'}
			type={type}
			fullWidth={true}
			size={size}
		/>
	);
};

export const Text = withLayout(TextComponent, true);

import { ComponentConfig } from '@prejt/ui-core';

const gridCol = (label: string, value: string) => ({
	name: 'grid-col',
	attributes: {
		label
	},
	childConfigs: [
		{
			name: 'string',
			attributes: {
				value
			}
		}
	]
});

export const section1Response: ComponentConfig<unknown> = {
	name: 'module',
	attributes: {},
	childConfigs: [
		{
			name: 'subscribe',
			attributes: {
				listenTo: 'dialog-close-requested',
				onEvent: 'close'
			}
		},
		{
			name: 'grid',
			attributes: {
				items: '/section1/items'
			},
			childConfigs: [
				{
					name: 'grid-row',
					attributes: {},
					childConfigs: [
						gridCol('Id', './id'),
						gridCol('Name', './name'),
						{
							name: 'grid-col',
							attributes: {},
							childConfigs: [
								{
									name: 'button',
									attributes: {
										label: 'Detail',
										onClick: 'value(./)|open(http://localhost/modal)'
									}
								}
							]
						}
					]
				}
			]
		},
		{
			name: 'button',
			attributes: {
				label: 'Create',
				variant: 'floating',
				icon: 'add',
				color: 'primary',
				onClick: 'value({})|wrap(params)|open(http://localhost/modal)'
			}
		}
	]
};

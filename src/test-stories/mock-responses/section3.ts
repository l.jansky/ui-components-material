import { ComponentConfig } from '@prejt/ui-core';

export const section3Response: ComponentConfig<unknown> = {
	name: 'module',
	attributes: {},
	childConfigs: [
		{
			name: 'box',
			attributes: {
				label: 'Section 3'
			}
		}
	]
};

import { ComponentConfig } from '@prejt/ui-core';
import { getTestContent } from '../../utils';

const content = getTestContent(200);

export const section4Response: ComponentConfig<unknown> = {
	name: 'module',
	attributes: {},
	childConfigs: [
		{
			name: 'box',
			attributes: {
				label: 'Section 4'
			},
			childConfigs: content
		}
	]
};

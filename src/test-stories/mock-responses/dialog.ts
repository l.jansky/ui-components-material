import { ComponentConfig } from '@prejt/ui-core';

const tab1Content: ComponentConfig[] = [
	{
		name: 'text',
		attributes: {
			label: 'Name',
			value: './fetched/name'
		}
	},
	{
		name: 'date',
		attributes: {
			label: 'Date',
			value: './fetched/date'
		}
	}
];

const listGridCol = (label: string, value: string): ComponentConfig => ({
	name: 'grid-col',
	attributes: {
		label
	},
	childConfigs: [
		{
			name: 'string',
			attributes: {
				value,
				enabled: '../../editing|eq(./id)|not'
			}
		},
		{
			name: 'text',
			attributes: {
				value,
				label,
				enabled: '../../editing|eq(./id)'
			}
		}
	]
});

const tab2Content: ComponentConfig[] = [
	{
		name: 'grid',
		attributes: {
			items: './fetched-list'
		},
		childConfigs: [
			{
				name: 'grid-row',
				attributes: {},
				childConfigs: [
					listGridCol('Id', './id'),
					listGridCol('Name', './name'),
					{
						name: 'grid-col',
						attributes: {
							justify: 'flex-end',
							spacing: '1'
						},
						childConfigs: [
							{
								name: 'button',
								attributes: {
									label: 'Edit',
									onClick: 'value(./id)|store(../../editing)',
									variant: 'icon',
									icon: 'edit',
									enabled: '../../editing|eq(./id)|not'
								}
							},
							{
								name: 'button',
								attributes: {
									label: 'Save',
									onClick: 'value|store(../../editing)',
									variant: 'icon',
									icon: 'save',
									enabled: '../../editing|eq(./id)'
								}
							},
							{
								name: 'button',
								attributes: {
									label: 'Delete',
									onClick: 'log',
									variant: 'icon',
									icon: 'delete'
								}
							}
						]
					}
				]
			}
		]
	}
];

export const dialogResponse: ComponentConfig = {
	name: 'module',
	attributes: {},
	childConfigs: [
		{
			name: 'fetcher',
			attributes: {
				fetch: '//detail/:id',
				onData: 'store(./fetched)',
				params: './id|wrap(id)',
				autofetch: 'true',
				enabled: './id'
			}
		},
		{
			name: 'fetcher',
			attributes: {
				fetch: '//list/:id',
				onData: 'store(./fetched-list)',
				params: './id|wrap(id)',
				autofetch: 'true',
				enabled: './id'
			}
		},
		{
			name: 'dialog',
			attributes: {
				label: 'Modal',
				visible: '1',
				onClose: 'path(./)|emit(dialog-close-requested)',
				full: 'true'
			},
			childConfigs: [
				{
					name: 'tabs',
					attributes: {
						variant: 'app-bar',
						label: 'Detail of something'
					},
					childConfigs: [
						{
							name: 'tab',
							attributes: {
								label: 'Form'
							},
							childConfigs: tab1Content
						},
						{
							name: 'tab',
							attributes: {
								label: 'List',
								enabled: './id'
							},
							childConfigs: tab2Content
						}
					]
				},
				{
					name: 'button',
					attributes: {
						label: 'Save',
						variant: 'text',
						color: 'primary',
						onClick: 'action(save)'
					}
				},
				{
					name: 'button',
					attributes: {
						label: 'Cancel',
						variant: 'text',
						onClick: 'path(./)|emit(dialog-close-requested)'
					}
				}
			]
		}
	]
};

import { prejtStory, getItems } from '../utils';
import { section1Response } from './mock-responses/section1';
import { section2Response } from './mock-responses/section2';
import { section3Response } from './mock-responses/section3';
import { section4Response } from './mock-responses/section4';
import { dialogResponse } from './mock-responses/dialog';

export default {
	title: 'Base app'
};

const section1Items = getItems(5, { id: '', name: 'Item' });
const listItems = getItems(8, { id: '', name: 'List item' });

export const WithMenuAndList = prejtStory(
	[
		{
			name: 'app-bar',
			attributes: {},
			childConfigs: [
				{
					name: 'button',
					attributes: {
						label: 'Toolbar button',
						onClick: 'storeToggle(/menu/visible)',
						variant: 'icon',
						color: 'inherit',
						icon: 'menu'
					}
				},
				{
					name: 'string',
					attributes: {
						value: 'Section name'
					}
				}
			]
		},
		{
			name: 'box',
			attributes: {
				label: 'Test section',
				module: '/module'
			},
			childConfigs: []
		},
		{
			name: 'drawer',
			attributes: {
				visible: '/menu/visible',
				anchor: 'left',
				onClose: 'storeToggle(/menu/visible)'
			},
			childConfigs: [
				{
					name: 'menu',
					attributes: {
						items: '/menu/items'
					},
					childConfigs: [
						{
							name: 'menu-item',
							attributes: {
								label: './label',
								onClick: 'value(./module)|store(/module)'
							}
						}
					]
				}
			]
		}
	],
	{
		menu: {
			visible: false,
			items: [
				{ label: 'Section 1', module: 'http://localhost/section1' },
				{ label: 'Section 2', module: 'http://localhost/section2' },
				{ label: 'Section 3', module: 'http://localhost/section3' },
				{ label: 'Section 4', module: 'http://localhost/section4' }
			]
		},
		module: 'http://localhost/section1',
		section1: {
			items: section1Items
		}
	}
)
	.fetchMock('http://localhost/section1', { body: section1Response })
	.fetchMock('http://localhost/section2', { body: section2Response })
	.fetchMock('http://localhost/section3', { body: section3Response })
	.fetchMock('http://localhost/section4', { body: section4Response })
	.fetchMock('http://localhost/modal', { body: dialogResponse })
	.fetchMock('express:/detail/:id', (url: string) => {
		const value = parseInt(url.split('/').pop() || '0');
		return { body: section1Items[value] };
	})
	.fetchMock('express:/list/:id', () => {
		return { body: listItems };
	});

export const WithAlternativeRouting = prejtStory(
	[
		{
			name: 'app-bar',
			attributes: {},
			childConfigs: [
				{
					name: 'button',
					attributes: {
						label: 'Toolbar button',
						onClick: 'storeToggle(/menu/visible)',
						variant: 'icon',
						color: 'inherit',
						icon: 'menu'
					}
				},
				{
					name: 'string',
					attributes: {
						value: 'Section name'
					}
				}
			]
		},
		{
			name: 'box',
			attributes: {
				label: 'Test section',
				visible: '/module|eq(section1)'
			},
			childConfigs: section1Response.childConfigs
		},
		{
			name: 'box',
			attributes: {
				label: 'Test section',
				visible: '/module|eq(section2)'
			},
			childConfigs: section2Response.childConfigs
		},
		{
			name: 'box',
			attributes: {
				label: 'Test section',
				visible: '/module|eq(section3)'
			},
			childConfigs: section3Response.childConfigs
		},
		{
			name: 'box',
			attributes: {
				label: 'Test section',
				visible: '/module|eq(section4)'
			},
			childConfigs: section4Response.childConfigs
		},
		{
			name: 'drawer',
			attributes: {
				visible: '/menu/visible',
				anchor: 'left',
				onClose: 'storeToggle(/menu/visible)'
			},
			childConfigs: [
				{
					name: 'menu',
					attributes: {
						items: '/menu/items'
					},
					childConfigs: [
						{
							name: 'menu-item',
							attributes: {
								label: './label',
								onClick: 'value(./module)|store(/module)'
							}
						}
					]
				}
			]
		}
	],
	{
		menu: {
			visible: false,
			items: [
				{ label: 'Section 1', module: 'section1' },
				{ label: 'Section 2', module: 'section2' },
				{ label: 'Section 3', module: 'section3' },
				{ label: 'Section 4', module: 'section4' }
			]
		},
		module: 'section1'
	}
);

import React from 'react';
import { PrejtComponent, useStoreDataPath, DataValue } from '@prejt/ui-core';
import { Grid, makeStyles } from '@material-ui/core';

export type LayoutProps = {
	lg?: string;
	md?: string;
	sm?: string;
	xl?: string;
	xs?: string;
};

type WidthValue = undefined | 1 | 2 | 3;

const parseWidthValue = (value: DataValue): WidthValue => {
	let parsedValue;

	if (typeof value === 'number') {
		parsedValue = value;
	}

	if (typeof value === 'string') {
		parsedValue = parseInt(value, 10);
	}

	if (parsedValue) {
		const allowedValues = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
		return allowedValues.includes(parsedValue)
			? (parsedValue as WidthValue)
			: undefined;
	}
};

const useStyles = makeStyles(theme => ({
	root: {
		paddingRight: theme.spacing(1)
	}
}));

export const withLayout = (
	Component: PrejtComponent<any>,
	defaultPadding = false
): PrejtComponent<LayoutProps> => ({
	parentDataPath,
	eventContext,
	lg,
	md,
	sm,
	xl,
	xs,
	...props
}) => {
	const [lgValue] = useStoreDataPath(lg, parentDataPath, eventContext);
	const [mdValue] = useStoreDataPath(md, parentDataPath, eventContext);
	const [smValue] = useStoreDataPath(sm, parentDataPath, eventContext);
	const [xlValue] = useStoreDataPath(xl, parentDataPath, eventContext);
	const [xsValue] = useStoreDataPath(xs, parentDataPath, eventContext);
	const classes = useStyles();

	return (
		<Grid
			item={true}
			lg={parseWidthValue(lgValue)}
			md={parseWidthValue(mdValue)}
			sm={parseWidthValue(smValue)}
			xl={parseWidthValue(xlValue)}
			xs={parseWidthValue(xsValue)}
			className={defaultPadding ? classes.root : undefined}
		>
			<Component
				{...props}
				parentDataPath={parentDataPath}
				eventContext={eventContext}
			/>
		</Grid>
	);
};

type ItemDataTemplate = {
	[key: string]: number | string;
};

const defaultItem = {
	value: 0,
	name: 'Item'
};

export const getItems = (
	count: number,
	dataTemplate: ItemDataTemplate = defaultItem
): ItemDataTemplate[] => {
	const items: ItemDataTemplate[] = [];

	for (let i = 0; i < count; i++) {
		const item = { ...dataTemplate };
		for (const key in item) {
			if (typeof item[key] === 'number') {
				item[key] = (item[key] as number) + i;
			}

			if (typeof item[key] === 'string') {
				item[key] = item[key] === '' ? i.toString() : `${item[key]} ${i}`;
			}
		}
		items.push(item);
	}

	return items;
};

import { ComponentConfig } from '@prejt/ui-core';

const options = [
	{
		name: 'option',
		attributes: {
			value: '1',
			label: 'Option 1'
		}
	},
	{
		name: 'option',
		attributes: {
			value: '2',
			label: 'Option 2'
		}
	}
];

const getRandomComponent = (i: number, exclude: string[], only?: string[]) => {
	const components: ComponentConfig<any>[] = [
		{
			name: 'text',
			attributes: {
				label: `Test label ${i + 1}`,
				value: `Test value`
			}
		},
		{
			name: 'date',
			attributes: {
				label: `Test label ${i + 1}`,
				value: '2020-01-25'
			}
		},
		{
			name: 'date-time',
			attributes: {
				label: `Test label ${i + 1}`,
				value: '2020-01-25 08:50:00'
			}
		},
		{
			name: 'autocomplete',
			attributes: {
				label: `Test label ${i + 1}`,
				value: '0',
				selected: 'Selected value'
			},
			childConfigs: options
		},
		{
			name: 'select',
			attributes: {
				label: `Test label ${i + 1}`,
				value: '0',
				selected: 'Selected value'
			},
			childConfigs: options
		},
		{
			name: 'button',
			attributes: {
				label: `Test label ${i + 1}`,
				onClick: 'action(button)'
			}
		},
		{
			name: 'checkbox',
			attributes: {
				label: `Test label ${i + 1}`,
				value: '1'
			}
		},
		{
			name: 'switch',
			attributes: {
				label: `Test label ${i + 1}`,
				value: '1'
			}
		},
		{
			name: 'box',
			attributes: {
				label: `Test label ${i + 1}`
			},
			childConfigs: []
		},
		{
			name: 'grid',
			attributes: {
				label: `Test label ${i + 1}`
			},
			childConfigs: options.map(opt => ({
				name: 'grid-row',
				attributes: {},
				childConfigs: [
					{
						name: 'grid-col',
						attributes: {
							label: 'Value'
						},
						childConfigs: [
							{
								name: 'string',
								attributes: {
									value: opt.attributes.value
								}
							}
						]
					},
					{
						name: 'grid-col',
						attributes: {
							label: 'Label'
						},
						childConfigs: [
							{
								name: 'string',
								attributes: {
									value: opt.attributes.label
								}
							}
						]
					}
				]
			}))
		}
	];

	const usedComponents = components.filter(component =>
		only ? only.includes(component.name) : !exclude.includes(component.name)
	);

	const randomIndex = Math.floor(Math.random() * usedComponents.length);
	return usedComponents[randomIndex];
};

export const getTestContent = (
	numberOfItems = 5,
	exclude: string[] = [],
	only?: string[]
): ComponentConfig<any>[] => {
	const items: ComponentConfig<any>[] = [];
	for (let i = 0; i < numberOfItems; i++) {
		items.push(getRandomComponent(i, exclude, only));
	}
	return items;
};

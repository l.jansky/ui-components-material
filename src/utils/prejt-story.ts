import { getPrejtStory } from '@prejt/storybook';
import { components, MuiComponentsProvider } from '../components';

export const prejtStory = getPrejtStory(components, MuiComponentsProvider);

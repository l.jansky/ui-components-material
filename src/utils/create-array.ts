export const createArray = (count: number, mapFce: (index: number) => any) => {
	return Array(count)
		.fill(null)
		.map((_, index) => mapFce(index));
};
